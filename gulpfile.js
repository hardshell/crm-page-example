'use strict';

const PROJECT_NAME = 'New Project';                                          // Указываем название нашего проекта

const gulp = require('gulp');                                                  // Подключаем Gulp
const sass = require('gulp-sass');                                             // для компиляции sass/scss => css
const path = require('path');
const pug = require('gulp-pug');                                               // для компиляции pug => html
const uglify = require('gulp-uglifyjs');                                       // для минимизации js файлов
const babel = require('gulp-babel');                                           // ES6 => ES5
const sourcemaps = require('gulp-sourcemaps');                                 // для создания сорсмапов js и css файлов
const autoprefixer = require('gulp-autoprefixer');                             // для добавления префиксов в css
const svgSprite = require("gulp-svg-sprite");                                  // для создания svg спрайтов
const rename = require('gulp-rename');                                         // для переименования файлов
const concat = require('gulp-concat');                                         // для конкатенации файлов
const notify = require('gulp-notify');                                         // уведомления об ошибках
const remember = require('gulp-remember');
const resolver = require('stylus').resolver;                                   // запоминаем путь для картинок
const gulpIf = require('gulp-if');                                             // для использования оператора if else
const rimraf = require('rimraf');                                              // для удаления папок/файлов

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';  // определяем продакшен или девелопмент

const webpack = require('gulp-webpack');                                       // Gulp Webpack
const gutil = require('gulp-util');

const paths = {                                                                // Создаём обьект для удобного редактирования всех путей в нашем проекте
  build: {                                                                     // Пути нашего билда
    html: './',
    js: './js/',
    css: './css/',
    img: './img/',
    fonts: './fonts/'
  },
  src: {                                                                       // Пути наших сорсов
    pug: ['./sources/views/*/*.pug', './sources/views/*.pug'],                 // 1-й путь для генерации всех файлов в корне папки partials, 2-й путь для генерации
    pugW: './sources/views/**/*.pug',
    js: './sources/js/**/**/**/**/*.js',
    libsDir: './sources/js/libs/*.js',
    libs: './sources/libs/*.js',                                               // Собираем все js библиотеки в libs.min.js
    style: './sources/styles/**/**/**/*.{sass,scss,css}',
    img: './sources/img/**/**/**/*.{jpg,png,gif,svg,jpeg,mp4}',
    fonts: './sources/fonts/**/**/**/*.{ttf,woff,woff2,eot,svg}'
  },
  clean: {                                                                     // Пути которые мы очищаем перед компиляцией
    html: './*.html',
    js: './js',
    css: './css',
    img: './img',
    fonts: './fonts'
  }
};

/********************************************************************************
 * {sass,scss,css} & sourcemaps
 ********************************************************************************/
gulp.task('sass', () => {                                                      // Компилируем sass/scss в css
  return gulp.src(paths.src.style)                                             // Выбираем файлы по нужному пути
    .pipe(gulpIf(isDev, sourcemaps.init()))                                    // Инициализируем sourcemap
    .pipe(sass({                                                               // Настройки sass/scss
      outputStyle: 'compressed',
      define: {url: resolver()}
    }).on('error', notify.onError()))
    .pipe(autoprefixer('last 3 version', '> 1%', 'IE 9', {cascade: true}))     // Настройки autoprefixer
    .pipe(gulpIf(isDev, sourcemaps.write('./')))                               // Записываем sourcemap
    .pipe(gulp.dest(paths.build.css));                                         // Выгружаем готовые css файлы в папку указанную в paths.build.css
});

/********************************************************************************
 * pug
 ********************************************************************************/
gulp.task('pug', () => {                                                       // Компилируем pug в html
  return gulp.src(paths.src.pug)                                               // Выбираем файлы по нужному пути
    .pipe(pug({                                                                // Настройки pug
      pretty: true,
    })).on('error', notify.onError())
    .pipe(rename({extname: ".html"}))
    .pipe(gulp.dest(paths.build.html));                                        // Выгружаем готовый html в папку указанную в paths.build.html
});

/********************************************************************************
 * images
 ********************************************************************************/
gulp.task('img', () => {                                                       // Копирование и оптимизация всех изображений
  return gulp.src(paths.src.img)                                               // Выбираем картинки по нужному пути
    .pipe(gulp.dest(paths.build.img))                                          // Выгружаем их в папку указанную в paths.build.img
});

/********************************************************************************
 * javascript libraries
 ********************************************************************************/
gulp.task('libs', () => {                                                      // Создание минимизированого файла(libs.min.js) состоящего из js библиотек которые находятся sources/libs
  return gulp.src(paths.src.libs)                                              // Выбираем файлы по нужному пути
    .pipe(concat('libs.min.js'))                                               // Собираем их в кучу в новом файле libs.min.js
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(uglify())                                                             // Сжимаем JS файл
    .pipe(gulp.dest(paths.build.js));                                           // Выгружаем в папку указанную в paths.build.js
});

/********************************************************************************
 * custom javascript
 ********************************************************************************/
gulp.task('javascript', function () {                                          // Создание минимизированого файла скриптов из sources/js
  return gulp.src([                                                            // Выбираем файлы по нужному пути
    'sources/js/core.js',
    'sources/js/modules/**/**/**/*.js'
  ])
    .pipe(webpack({
      output: {
        path: require("path").resolve("./sources/js"),
        filename: 'core.js'
      },
      module: {
        loaders: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader"
          }
        ]
      }
    }))
    .pipe(gulp.dest(paths.build.js))
    .pipe(sourcemaps.init({loadMaps: true}))                                   // Инициализируем sourcemap
    .pipe(uglify({                                                             // Настройки js
      mangle: false,
      compress: false,
      preserveComments: false
    }))
    .on('error', gutil.log)
    .pipe(concat('core.min.js'))                                               // Собираем их в кучу в новом файле core.min.js
    .pipe(sourcemaps.write(paths.build.html))                                  // Записываем sourcemap
    .pipe(uglify())
    .pipe(gulp.dest(paths.build.js));                                          // Выгружаем в папку
});

/********************************************************************************
 * svg-sprite html
 ********************************************************************************/
gulp.task('svg', () => {                                                       // Создание svg-symbol спрайта который вставляется в html и вызывается с помощью use
  return gulp.src('./sources/svgs/html/*.svg')
    .pipe(svgSprite({                                                          // Настройки svg symbol
      mode: {
        symbol: {
          prefix: '',
          dist: './',
          inline: true,
          sprite: './svg-sprite.svg'
        }
      }
    }))
    .on('error', error => {
      console.log(error)
    })
    .pipe(gulp.dest(paths.build.img));                                         // Выгружаем в папку указанную в paths.build.img
});

/********************************************************************************
 * svg-sprite css
 ********************************************************************************/
gulp.task('svg-css', () => {                                                   // Создание свг спрайта из свг файлов которые находятся sources/svgs/css
  return gulp.src('./sources/svgs/css/*.svg')
    .pipe(svgSprite({                                                          // Настройки svg sprite
      mode: {
        css: {
          dest: '.',
          sprite: 'svg-sprite.svg',
          layout: 'vertical',
          prefix: '.i-',
          dimensions: true,
          render: {
            scss: {
              dest: '_sprite.scss'
            }
          }
        }
      }
    }))
    .on('error', error => {
      console.log(error);
    })
    .pipe(gulpIf('*.scss', gulp.dest('./sources/styles/003-patterns'), gulp.dest(paths.build.css)));
});

/********************************************************************************
 * clean
 ********************************************************************************/
gulp.task('clean', cb => {                                                     // Удаление всех билдов перед новой компиляцией

  let cl = paths.clean;

  rimraf(cl.img, cb);
  rimraf(cl.html, cb);
  rimraf(cl.css, cb);
  rimraf(cl.js, cb);
  rimraf(cl.fonts, cb);

});

/********************************************************************************
 * watch
 ********************************************************************************/
gulp.task('watch', () => {                                                     // Запуск вотчера (gulp watch)

  let filepath = filepath => {
      remember.forget('svg-css', path.resolve(filepath))
    },
    src = paths.src;

  gulp.watch([src.style], gulp.series('sass')).on('unlink', filepath);
  gulp.watch([src.pugW], gulp.series('pug')).on('unlink', filepath);
  gulp.watch([src.img], gulp.series('img')).on('unlink', filepath);
  gulp.watch([src.js], gulp.series('javascript')).on('unlink', filepath);
  gulp.watch(['./sources/svgs/html/*.svg'], gulp.series('svg')).on('unlink', filepath);
  gulp.watch(['./sources/svgs/css/*.svg'], gulp.series('svg-css')).on('unlink', filepath);

});

/********************************************************************************
 * Copy files/folders
 ********************************************************************************/
gulp.task('copy', () => {                                                      // Копирование файлов (gulp copy)
  return gulp.src(paths.src.fonts)                                             // Выбираем файлы по нужному пути
    .pipe(gulp.dest(paths.build.fonts))                                        // Выгружаем шрифты в папку указанную в paths.build.fonts
});

/*******************************************************************************
 * build
 *******************************************************************************/
gulp.task('build', gulp.series(                                                // Таска для создания билдов
  'clean', 'sass', 'libs', 'javascript', 'svg', 'svg-css', 'img', 'copy', 'pug'),
  'watch'
);

/********************************************************************************
 * default
 ********************************************************************************/
gulp.task('default', gulp.series('build',                                      // Дефолтная таска для билдинга, запуска сервера и вотчера (gulp)
  gulp.parallel('watch'))
);

// и главное - от кодинга надо получать удовольствие!