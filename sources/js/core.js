'use strict';

($ => {

  const app = {

    init: () => {

      app.interactive();

      $(window).resize(() => {
        app.resizeEvents();
      }).resize();

      $(window).scroll(() => {
        app.scrollEvents();
      }).scroll();
    },

    interactive: () => {

    },

    resizeEvents: () => {

    },

    scrollEvents: () => {

    }
  };

  $(document).ready(() => {
    app.init();
  });

})(jQuery);